const { client } = require('nightwatch-api');
const { Given, Then, When } = require('cucumber');

Given('I open Google search page', function () {
    // Write code here that turns the phrase above into concrete actions
    return client.url('http://google.com').waitForElementVisible('body', 1000);
});


Then(/^the title is "([^"]*)"$/, title => {
    return client.assert.title(title);
});

Then(/^the Google search form exists$/, () => {
    return client.assert.visible('input[name="q"]');
});
