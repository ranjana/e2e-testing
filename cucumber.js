
let defaultRequirements = [
    '--require cucumber.conf.js',
    '--require src',
    '--format node_modules/cucumber-pretty',
    '--require features/',
    '--format json:report/cucumber_report.json',
];
defaultRequirements = defaultRequirements.join(' ');

module.exports = {
    default: defaultRequirements
};
